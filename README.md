# quarto

![Logo](docs/assets/logo.png)

The image includes [quarto](https://quarto.org/) and [R](https://www.r-project.org/), it was created to be used in CI/CD pipelines.

> :loud_sound: **Announcement 2024-10-08**
> 
> Project moved from `https://gitlab.com/kamapu/quarto-image` to `https://gitlab.com/kamapu/oci/images/quarto`. The path of a container repository is always the same as the repository path of the associated project, so it is not possible to rename or move just the container registry.
> 
> So please update your registry references from `registry.gitlab.com/kamapu/quarto-image:<IMAGE_TAG>` to `registry.gitlab.com/kamapu/oci/images/quarto:<IMAGE_TAG>`.

## How can your Quarto project be rendered?

Mount your project data under the container path `/data` and render your quarto project.

The following Docker command executes a container image with certain configuration options. Here's an explanation of each part of the command:

- `docker run`: This is the command used to run a Docker container.
- `--rm`: This option causes the container to be automatically removed after it exits. This means the container is not persistent and leaves no traces after it terminates.
- `-u $(stat -c '%u:%g' $(pwd))`: This option sets the user and group within the container to the user and group of the current directory (`$(pwd)`). This allows the container to execute files with the same permissions as the user this directory.
- `-v $(pwd):/data`: This option creates a volume mapping where the current directory (`$(pwd)`) is mapped to the `/data` directory in the container. This allows the container to access files and folders in the current directory.
- `registry.gitlab.com/kamapu/oci/images/quarto:latest`: This is the name and version of the Docker image to be run. You can also change `latest` to a specific version.
- `bash -c "cd /data && quarto render . --output-dir=public/`: This is the command to be executed within the container. It first changes to the `/data` directory in the container and then executes the `quarto render . --output-dir=public/` command. This command renders Quarto documents in the current directory and saves the output in the `public/` directory.

```bash
docker run \
  --rm \
  -u $(stat -c '%u:%g' $(pwd)) \
  -v $(pwd):/data \
  registry.gitlab.com/kamapu/oci/images/quarto:latest \
  bash -c "cd /data && quarto render . --output-dir=public/"

# Open generated html in browser
open public/index.html
```

If further packages are required in a project in addition to the [pre-installed R packages](image/r-preinstalled-packages.txt), these can be installed later, e.g.:

```bash
docker run \
  --rm \
  -u $(stat -c '%u:%g' $(pwd)) \
  -v $(pwd):/data \
  registry.gitlab.com/kamapu/oci/images/quarto:latest \
  bash -c "R -e \"install.packages('remotes')\" && cd /data && quarto render . --output-dir=public/"

# Open generated html in browser
open public/index.html
```
