# Used by `image`, `push` & `deploy` targets, override as required
IMAGE_REG ?= registry.gitlab.com
IMAGE_REPO ?= kamapu/oci/images/quarto
IMAGE_TAG ?= latest
CI_IMAGE_TAG ?= replace_me

IMAGE_ARG_BUILD_DATE ?= $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
IMAGE_ARG_GIT_COMMIT ?= $(shell git rev-parse --short HEAD)

.PHONY: .EXPORT_ALL_VARIABLES
.DEFAULT_GOAL := help

.PHONY: help
help:  ## 💬 This help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: lint
lint: ## 🔎 Lint Dockerfile
	hadolint image/Dockerfile

.PHONY: build
build:  ## 🔨 Build container image from Dockerfile
	docker build . --file image/Dockerfile \
	--build-arg VERSION=$(IMAGE_TAG) \
	--build-arg BUILD_DATE=$(IMAGE_ARG_BUILD_DATE) \
	--build-arg GIT_COMMIT=$(IMAGE_ARG_GIT_COMMIT) \
	--tag $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

.PHONY: pull
pull:  ## 📥 Pull container image from registry
	docker pull $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

.PHONY: ci-tag-image
ci-tag-image:  ## 🏷️  Tag container image to final tag
	docker tag $(IMAGE_REG)/$(IMAGE_REPO):$(CI_IMAGE_TAG) $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

.PHONY: tag-latest
tag-latest:  ## 🏷️  Tag container image as latest
	docker tag $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG) $(IMAGE_REG)/$(IMAGE_REPO):latest

.PHONY: push
push:  ## 📤 Push container image to registry
	docker push $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

.PHONY: push-latest
push-latest:  ## 📤 Push container image to registry (tag: latest)
	docker push $(IMAGE_REG)/$(IMAGE_REPO):latest

.PHONY: test
test: ## 🎯 Unit tests Image
	echo "TODO tests"
	exit 1
