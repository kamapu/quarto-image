#!/usr/bin/env bash

set -e

mkdir -p /tmp/etc
cp -a /etc/passwd.origin /tmp/etc/passwd
cp -a /etc/group.origin /tmp/etc/group

uid="$(id -u)"
gid="$(id -g)"

sed -i "s/^\(non-root:x\):\([^:]\+\):\([^:]\+\):\(.*\)/\1:${uid}:${gid}:\4/" /tmp/etc/passwd
sed -i "s/^\(non-root:x\):\([^:]\+\):/\1:${gid}:/" /tmp/etc/group

# Creates home directory for the unprivileged user,
# it must be ensured that the directory exists in a writable directory
mkdir -p /tmp/home/non-root

sudo /usr/local/lib/finalize-entrypoint.sh

if [ "$uid" == "0" ]; then
    exec "$@"
else
    HOME=/tmp/home/non-root /usr/local/lib/finalize-R-environment.sh
    # Sets HOME to ensure that the created directory is used for the unprivileged user.
    HOME=/tmp/home/non-root exec "$@"
fi
