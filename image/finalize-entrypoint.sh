#!/usr/bin/env bash

set -e

# This script meant to be called as last action from entrypoint script only.
# Main purpose of this script is to restore the file ownerships and permissions
# of the redirected /etc/passwd and /etc/group to secure values.

chown root:root /tmp/etc/passwd /tmp/etc/group
chmod 644 /tmp/etc/passwd /tmp/etc/group
